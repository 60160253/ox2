import java.util.*;
public class Game {
	
	private Board board;
	private Player X;
	private Player O;
	private Player win;
	private Player draw;
	
	public Game() {
		X = new Player("X");
		O = new Player("O");
		board = new Board(X, O);
	}
	
	public void play() {
		
		showWelcome();
		showTable();
		showTurn();
		showPosition();
		input();
	}
	
	private void showPosition() {
		System.out.println("Plz choose position (R,C): ");
	}

	private void showWelcome() {
		System.out.println("Start Game OX");
	}
	
	private void showTable() {
		String[][] table = board.getTable();
		System.out.println("  1 2 3");
		for(int i=0; i<table.length; i++) {
			System.out.print(i+1);
			for(int j=0; j<table.length; j++ ) {
				System.out.print("|" + table[i][j]);
			}
			System.out.print("|");
			System.out.println();
		}
	}
	
	private void showTurn() {
		Player player = board.getCurrentPlayer();
		System.out.println("Turn: " + player.getName());
	}
	
	private void input() {
		Scanner kb = new Scanner(System.in);
		win = null;
		Player player = board.getCurrentPlayer();
		while (win == null) {
			int numInR = 0;
			int numInC = 0;
			try {
				numInR = kb.nextInt();
				numInC = kb.nextInt();
				if(!(numInC>0 && numInC <=3) || !(numInR>0 && numInR<=3)) {
					System.out.println("Row and Column must be number 1 - 3");
					continue;
				}
			}catch (InputMismatchException e) {
				System.out.println("Row and Column must be number");
			}
			if(board.getTable()[numInR-1][numInC-1] != "O" && board.getTable()[numInR-1][numInC-1] != "X") {
				board.getTable()[numInR-1][numInC-1] = player.getName();
				board.switchPlayer();
				board.getTable();
				win = board.checkWin();
			}else {
				System.out.print("Row "+numInR+" and Column "+(numInC-1)+" can't choose again");
				continue;
			}
		}
		if (win == draw) {
			System.out.println("DRAW");
		} else {
			System.out.println(win + " : WIN");
		}
	}
}
